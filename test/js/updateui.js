var button = document.getElementById('jsapi');

if (button) {
    button.addEventListener('click', function () {
        javacalljs();
    }, false);
}
    
// call Java method through JavaScript API.
function javacalljs() {
    jsapi.javacalljs(function success() {
        alert('success callback.');
    }, function error() {
        alert('error callback.');
    });
}

// updateui.
function updateui(data) {
    alert('updateui has been called.');
    alert(data);
}