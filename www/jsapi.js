var cordova = require('cordova');
var exec = require('cordova/exec');

module.exports = {

	javacalljs: function(successCallback, errorCallback) {
		exec(successCallback, errorCallback, 'JavaCallJs', 'callback', []);
	},

};