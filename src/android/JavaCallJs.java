package cn.edu.nju;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;

public class JavaCallJs extends CordovaPlugin {
  
    private String TAG = "Cordova5TestPlugin";
  
    private Activity activity;
    
    private CordovaWebView webView;
    
    private String data;
    
    /*
     * Constructor
     */
    public JavaCallJs() {

    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
      Log.d(TAG, "initialize");
      super.initialize(cordova, webView);
      this.activity = cordova.getActivity();
      this.webView = webView;
      
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
      
        if (action.equals("callback")) {
          data = "<test data>";
          callback();
            return true;
        } 
        return false;
    }
    
    @JavascriptInterface
    public void callback() {
      // injectJS("alert('Java methond callback() has been called.');");
      injectJS("updateui('" + data + "');");
    }
    
    private void injectJS (final String js) {
      activity.runOnUiThread(new Runnable() {
        public void run() {
          webView.loadUrl("javascript:" + js);
        }
      });
    }
    
    private void fireJSEvent(String eventName, String message) {
      String js = "javascript:var e = document.createEvent('Events'); e.initEvent('" + eventName + "', true, true);e.message='" + message +"';document.dispatchEvent(e)";
      Log.d(TAG, js);
      injectJS(js);
    }
    
}