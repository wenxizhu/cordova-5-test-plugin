# 安装步骤：
1. cordova create hello
2. cd hello
3. cordova platform add android
4. 将插件目下下test文件夹中的所有内容（index.html,css,js和img）复制到hello工程的www目录下
5. cordova build
6. plugman install --platform android --project platforms/android --plugin <path-to-the-plugin>

此时已经可以将hello工程导入IDE中运行调试。